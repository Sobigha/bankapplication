package com.example.demo.validate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;
import com.example.demo.error.Error;

import com.example.demo.dto.AccountRequest;

@Component
public class AccountValidate {

	public List<Error> validate(AccountRequest form){
		
		List<Error> errors = new ArrayList<>();
		int flag1=1,flag2=1;
		
		if(form.getAccountHolderName() == null)
		{
			Error error = new Error("Account Holder Name", "Account Holder Name should not be empty");
			errors.add(error);
		}
		
		if(form.getAccountType() == null)
		{
			Error error = new Error("Account Type", "Account Type should not be empty");
			errors.add(error);
			flag1=0;
		}
		
		if(form.getDob()== null)
		{
			Error error = new Error("Date Of Birth", "Date Of Birth should not be empty");
			errors.add(error);
			flag2=0;
		}
		
		if(flag1 == 1)
		{
			if((form.getAccountType().toUpperCase().equals("SAVINGS")) || (form.getAccountType().toUpperCase().equals("CURRENT")))
			{
				
			}
			else
			{
				Error error = new Error("Account Type", "Account Type must be SAVINGS | CURRENT");
				errors.add(error);
			}
		}
	    
		Date todayDate = new Date();
	  
		if(flag2 == 1)
		{
		    if(form.getDob().compareTo(todayDate) == 1)
		    {
		    	Error error = new Error("Birth Date", "Date of Birth is not valid");
				errors.add(error);
		    }
		}
		return errors;
		
	}
	
}
