package com.example.demo.validate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.dto.TransactRequest;
import com.example.demo.error.Error;
import com.example.demo.model.BankAccount;
import com.example.demo.repo.BankRepo;

@Component
public class TransactValidate {
	 public List<Error> validate(TransactRequest form,Long number,BankRepo obj){
			
		    int flag=0,flag2=0;
		   
			List<Error> errors = new ArrayList<>();
			
			if(obj.existsById(number))
			{
				flag=1;
			}
			else
			{
				Error error = new Error("Account Number", "Account Number does not exist");
				errors.add(error);
				
			}
			
			
			if(flag == 1)
			{
				if(form.getType().toUpperCase().equals("DEPOSIT") || (form.getType().toUpperCase().equals("WITHDRAWAL")))
				{
					flag2 = 1;
				}
				else
				{
					Error error = new Error("Type", "Type should be DEPOSIT | WITHDRAWAL");
					errors.add(error);
				}
			}
			
			if(flag==1 && flag2==1)
			{
				BankAccount ob = obj.getById(number);
				if(form.getType().toUpperCase().equals("WITHDRAWAL"))
		    	{
		    		Double difference = ob.getBalance()-form.getAmount();
		    		if(difference < 0.0)
		    		{
						Error error = new Error("Minimum Balance", "Minimum balance should be atleast 0.0");
						errors.add(error);
		    		}
				}
					
			}		
			
		    return errors;	
		
		 }
}
