package com.example.demo.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
public class Transaction {
   

	   @Id
	   @GeneratedValue(strategy = GenerationType.IDENTITY)
	   private Long transactionId;	   
	   
	   @ManyToOne
	   @JoinColumn(name = "account_number")
	   private BankAccount transact;
	   
	   private Double transactionAmount;
	  
	   private String transactionType;
	   
	   private Double oldBalance;
	   private Double newBalance;
	   
	   @CreationTimestamp
	   private LocalDateTime transactionDate;
	  
	   private String transactionStatus;
	   
	   @CreationTimestamp
	   private LocalDateTime createdAt;
	   
	   @UpdateTimestamp
	   private LocalDateTime updatedAt;
	   
	   	 
		public Long getTransactionId() {
			return transactionId;
		}
	
		public void setTransactionId(Long transactionId) {
			this.transactionId = transactionId;
		}
	
		
		@JsonBackReference
		public BankAccount getTransact() {
			return transact;
		}
	
		public void setTransact(BankAccount transact) {
			this.transact = transact;
		}
	
		public Double getTransactionAmount() {
			return transactionAmount;
		}
	
		public void setTransactionAmount(Double transactionAmount) {
			this.transactionAmount = transactionAmount;
		}
	
		public String getTransactionType() {
			return transactionType;
		}
	
		public void setTransactionType(String transactionType) {
			this.transactionType = transactionType;
		}
	
		public Double getOldBalance() {
			return oldBalance;
		}
	
		public void setOldBalance(Double oldBalance) {
			this.oldBalance = oldBalance;
		}
	
		public Double getNewBalance() {
			return newBalance;
		}
	
		public void setNewBalance(Double newBalance) {
			this.newBalance = newBalance;
		}
	
	
	

		public LocalDateTime getTransactionDate() {
			return transactionDate;
		}

		public void setTransactionDate(LocalDateTime transactionDate) {
			this.transactionDate = transactionDate;
		}

		public String getTransactionStatus() {
			return transactionStatus;
		}
	
		public void setTransactionStatus(String transactionStatus) {
			this.transactionStatus = transactionStatus;
		}
	
		public LocalDateTime getCreatedAt() {
			return createdAt;
		}
	
		public void setCreatedAt(LocalDateTime createdAt) {
			this.createdAt = createdAt;
		}
	
		public LocalDateTime getUpdatedAt() {
			return updatedAt;
		}
	
		public void setUpdatedAt(LocalDateTime updatedAt) {
			this.updatedAt = updatedAt;
		}
	   
}
