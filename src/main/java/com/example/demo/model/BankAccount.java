package com.example.demo.model;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
public class BankAccount {
     
	      	@Id
	      	@GeneratedValue(strategy = GenerationType.IDENTITY)
	      	@Column(name = "account_number")
	      	private Long accountNumber;
	     
	      	private String accountHolderName;
	      
	      	private Date dateOfBirth;
	      
	      	private Double balance=0.0;
	      	private Double transactionFee = 0.0;
	      
	      	private String accountType;
	      
	      	@CreationTimestamp
	      	private LocalDateTime createdAt;
	      
	      	@UpdateTimestamp
	      	private LocalDateTime updatedAt;
	      
	      	@OneToMany(mappedBy = "transact" ,cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	      	private List<Transaction> transaction;
	      
	        @JsonManagedReference  
		    public List<Transaction> getTransaction() {
		    	return transaction;
		    }

		    public void setTransaction(List<Transaction> transaction) {
		    	this.transaction = transaction;
		    }

			public Long getAccountNumber() {
				return accountNumber;
			}
	
			public void setAccountNumber(Long accountNumber) {
				this.accountNumber = accountNumber;
			}

			public String getAccountHolderName() {
				return accountHolderName;
			}
	
			public void setAccountHolderName(String accountHolderName) {
				this.accountHolderName = accountHolderName;
			}
	
			public Date getDateOfBirth() {
				return dateOfBirth;
			}
	
			public void setDateOfBirth(Date dateOfBirth) {
				this.dateOfBirth = dateOfBirth;
			}
	
			public Double getBalance() {
				return balance;
			}
	
			public void setBalance(Double balance) {
				this.balance = balance;
			}
	
			public Double getTransactionFee() {
				return transactionFee;
			}
	
			public void setTransactionFee(Double transactionFee) {
				this.transactionFee = transactionFee;
			}
	
			public String getAccountType() {
				return accountType;
			}
	
			public void setAccountType(String accountType) {
				this.accountType = accountType;
			}
	
			public LocalDateTime getCreatedAt() {
				return createdAt;
			}
	
			public void setCreatedAt(LocalDateTime createdAt) {
				this.createdAt = createdAt;
			}
	
			public LocalDateTime getUpdatedAt() {
				return updatedAt;
			}
	
			public void setUpdatedAt(LocalDateTime updatedAt) {
				this.updatedAt = updatedAt;
			}

}
