package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.TransactRequest;
import com.example.demo.dto.TransactResponse;
import com.example.demo.error.BadRequest;
import com.example.demo.model.BankAccount;
import com.example.demo.model.Transaction;
import com.example.demo.repo.BankRepo;
import com.example.demo.repo.TransactRepo;
import com.example.demo.validate.TransactValidate;


@RestController
public class TransactController {
	  
		@Autowired
		private BankRepo repo;
	    
		@Autowired
		private TransactRepo transrepo;
		
		@Autowired
	    TransactValidate valid;
		

        static final Logger logger = LogManager.getLogger(BankController.class.getName());
    
	    @PostMapping(path = "/bank/account/{number}/transaction" , produces = {"application/json"})
	    public TransactResponse getAll(@RequestBody TransactRequest trans,@PathVariable Long number)
	    {	    	
	    	   	
	    	logger.info("Post Mapping of Transaction Request Begins");
	    	
	    	List<com.example.demo.error.Error> error = valid.validate(trans,number,repo);
	    	if(error.size() > 0)
	    	{
	    		logger.error("Error in Transaction Controller Happened");
	    		throw new BadRequest("bad request",error);
	    	}
	    	
	    	BankAccount obj = repo.getById(number);
	    	Transaction tobj = new Transaction();
	    	TransactResponse response = new TransactResponse();
	    	    	
	    	
	    	Double balance = obj.getBalance();	    			    	
	    	String accountType = obj.getAccountType();
	    	Double transactionFee = obj.getTransactionFee();
	
	    	if(trans.getType().toUpperCase().equals("WITHDRAWAL"))
	    	{
	    		Double difference = balance-trans.getAmount();
	    		if(difference >= 0.0)
	    		{
	    			tobj.setNewBalance(difference);
	    			tobj.setOldBalance(balance);
	    			obj.setBalance(difference);
	    			response.setNewBalance(difference);
	    			response.setOldBalance(balance);
	    			tobj.setTransactionStatus("SUCCESS");
	    			response.setTransactionStatus("SUCCESS");
	    				    			
	    		}
	    		else
	    		{
	    			tobj.setNewBalance(balance);
	    			tobj.setOldBalance(balance);
	    			obj.setBalance(balance);
	    			response.setNewBalance(balance);
	    			response.setOldBalance(balance);
	    		    tobj.setTransactionStatus("FAILURE");
	    		    response.setTransactionStatus("FAILURE");
	    		   
	    		}		    		
	    	}
	    	
	    	else if(trans.getType().toUpperCase().equals("DEPOSIT"))
	    	{
	    		obj.setBalance(balance+trans.getAmount());
	    		tobj.setOldBalance(balance);
	    		tobj.setNewBalance(balance+trans.getAmount());
	    		response.setOldBalance(balance);
	    		response.setNewBalance(balance+trans.getAmount());
	    		tobj.setTransactionStatus("SUCCESS");
    		    response.setTransactionStatus("SUCCESS");
	    		
	    	}
	    	
	    	if(accountType.toUpperCase().equals("CURRENT"))
	    		obj.setTransactionFee(transactionFee+5.0);
	    	
	    	
	    	
	    	tobj.setTransact(obj);		       
	    	tobj.setTransactionAmount(trans.getAmount());
	        tobj.setTransactionType(trans.getType());
	             	
	        repo.save(obj);
	        transrepo.save(tobj);
	        logger.info("Saving in repository");

	        response.setTransactionId(tobj.getTransactionId());
	        response.setAccountNumber(number);
	        response.setAmount(trans.getAmount());
	        response.setType(trans.getType());
	        response.setTransactionDate(tobj.getTransactionDate());
	        response.setCreatedAt(tobj.getCreatedAt());
	        response.setUpdatedAt(tobj.getUpdatedAt());
	      
	        logger.info("Return the response");
	    	  

	        return response;
    
      }
	    
	        
	    @GetMapping(path = "/bank/account/transaction" , produces = {"application/json"})
	    public List<Transaction> getAllMethod()
	    {	    	
			return transrepo.findAll(); 	    	
	    }	  
	    
	    @GetMapping(path = "/bank/account/{number}/transaction" , produces = {"application/json"})
	    public Optional<Transaction> getAllMethods(@PathVariable Long number)
	    {	    	
			return transrepo.findById(number);	    	
	    }		    
   
	    
}
    
 
    		   
