package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.AccountRequest;
import com.example.demo.dto.AccountResponse;
import com.example.demo.error.BadRequest;
import com.example.demo.model.BankAccount;
import com.example.demo.model.Transaction;
import com.example.demo.repo.BankRepo;
import com.example.demo.repo.TransactRepo;
import com.example.demo.validate.AccountValidate;


@RestController
public class BankController {
		
			@Autowired
			private BankRepo repo;
			
			@Autowired
			TransactRepo transrepo;
		 
			@Autowired
			AccountValidate valid;
			
			static final Logger logger = LogManager.getLogger(BankController.class.getName());
			
		    @PostMapping(path = "/bank/account", consumes = {"application/json"})
		    public AccountResponse addData(@RequestBody AccountRequest form)
		    {
		    	
		    	logger.info("Post Mapping of Account Request");
		    	
		    	Transaction tobj = new Transaction();
		    	AccountResponse response = new AccountResponse();    // dto response
		    	
		    	BankAccount obj = new BankAccount();                 // entity
		    	
		    	
		    	List<com.example.demo.error.Error> error = valid.validate(form);
		    	if(error.size() > 0)
		    	{
		    		 logger.error("Error in Bank Controller Happened");
		    		throw new BadRequest("bad request",error);
		    	}
		    	
		    	obj.setAccountHolderName(form.getAccountHolderName());
		    	response.setAccountHolderName(form.getAccountHolderName());
		    	
		    	obj.setDateOfBirth(form.getDob());
		    	response.setDateOfBirth(form.getDob());
		    	
		    	
		    	String accType = form.getAccountType().toUpperCase();	    	
		    	
		    	if(accType.equals("SAVINGS") || accType.equals("CURRENT"))
		    	{
		    		obj.setAccountType(accType);
		    	    response.setAccountType(accType);
		    	    
		    	}
		    	
		    	if(accType.equals("CURRENT"))
		    	{
		    		obj.setTransactionFee(obj.getTransactionFee()+5.0);
		    	    response.setTransactionFee(obj.getTransactionFee());
		    	}
		    	    	
		    	if(form.getInitialDeposit() != 0.0)
		    	{
		    		obj.setBalance(form.getInitialDeposit());
		    	    response.setBalance(form.getInitialDeposit());
		    	}
		    	   		    			    	
				repo.save(obj);
				logger.info("Saved in BankAccount Repository");
					
				response.setAccountNumber(obj.getAccountNumber());
				response.setCreatedAt(obj.getCreatedAt());
		    	response.setUpdatedAt(obj.getUpdatedAt());
		    	
		    	System.out.println(form.getInitialDeposit());
		    	
		    	if(form.getInitialDeposit() != 0.0 && form.getInitialDeposit()!= null)
		    	{
		    		
		    		logger.info("Consider Initial Deposit as Transaction");
		    		tobj.setTransactionStatus("SUCCESS");
		    		tobj.setOldBalance(0.0);
		    		tobj.setNewBalance(form.getInitialDeposit());
		    		tobj.setTransactionType("DEPOSIT");
		    		tobj.setTransactionAmount(form.getInitialDeposit());
		    		tobj.setTransact(obj);
		    		transrepo.save(tobj);
		    		
		    		Long num = obj.getAccountNumber();
		    		System.out.println(num);
		    		BankAccount bcc = repo.getById(num);
			    	Optional<Transaction> list = transrepo.findById(num);
			    	
		    			    	
			    	System.out.print(list);
//		    	    response.setTransaction(list);
		    		
		    	}
		    	
		    	logger.info("Return the response");
				return response;	       	
		     
		    }   
  
		    
		    @GetMapping(path = "/bank/account/" , produces = {"application/json"})
		    public List<BankAccount> get()
		    {	    	
				return repo.findAll(); 	    	
		    }
		    		    
		    	    
		    @GetMapping(path = "/bank/account/{number}" , produces = {"application/json"})
		    public Optional<BankAccount> getAllMethod(@PathVariable Long number)
		    {	    	
				return repo.findById(number); 	    	
		    }
		   		
		    
}

