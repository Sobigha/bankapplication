package com.example.demo.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.RecordRequest;
import com.example.demo.dto.RecordResponse;
import com.example.demo.model.BankAccount;
import com.example.demo.model.Transaction;
import com.example.demo.repo.BankRepo;
import com.example.demo.repo.TransactRepo;


@RestController
public class RecordController {

		@Autowired
		private BankRepo repo;
		
		@Autowired
		private TransactRepo transrepo;
		
		static final Logger logger = LogManager.getLogger(BankController.class.getName());

	    @GetMapping(path = "/bank/account/transactions/record/{number}" , produces = {"application/json"})
	    public RecordResponse getAllByDates(@PathVariable Long number,@RequestParam(value = "fromdate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromdate,
                                            @RequestParam(value = "todate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate todate)
	    {
	    	
//	    	System.out.println(rec.getFromDate()+"  "+rec.getFromDate());
	    	logger.info("Post Mapping of Transaction Records Request");
	    	
	    	BankAccount obj = repo.getById(number);
	    	List<Transaction> obj1 = obj.getTransaction();
	    	List<Transaction> listAdd = new ArrayList<>();
	    	
	    	LocalDate from;
	    	LocalDate to;
	    	
	    	if(fromdate != null)
	    	{
	    	  from = fromdate;
	    	}
	    	else
	    	{
	    		LocalDate ob = obj.getCreatedAt().toLocalDate();
	    		from = ob;
	    	}
	    	 
	    	if(todate != null)
	    	{
	    	    to = todate;
	 
	    	}
	    	else
	    	{
	    		to = LocalDateTime.now().toLocalDate();
	    	}
	    	 
	    	System.out.println(from);
	    	System.out.println(to);
	    	Transaction list = null;
	    	for(int i=0;i<obj1.size();i++)
	    	 {
	    		 LocalDateTime date = obj1.get(i).getTransactionDate();
	    		 LocalDate date1 = date.toLocalDate();
	    		 if((from.compareTo(date1) <= 0) && (to.compareTo(date1)>=0)) {
	    			list = obj1.get(i);
	    	    }
	    		if(list != null) {
	    		     listAdd.add(list);
	    		}
	    	 }
	         
	    	System.out.println(listAdd);
	    	logger.info("Set the response");
	    	RecordResponse response = new RecordResponse();
	    	response.setAccountNumber(number);
	    	response.setAccountHolderName(repo.findById(number).get().getAccountHolderName());
	    	response.setDateOfBirth(repo.findById(number).get().getDateOfBirth());
	    	response.setBalance(repo.findById(number).get().getBalance());
	    	response.setAccountType(repo.findById(number).get().getAccountType());
	    	response.setFromDate(from);
	    	response.setToDate(to);	
	    	response.setTransactions(listAdd);
	    	
	    	logger.info("Return the response");
			return response;	    	
			   	
	    }	
	    
	    
}
