package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import com.example.demo.model.Transaction;


public interface TransactRepo extends JpaRepository<Transaction, Long>{
    
	
}


