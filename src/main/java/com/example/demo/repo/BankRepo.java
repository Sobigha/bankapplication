package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.model.BankAccount;


public interface BankRepo extends JpaRepository<BankAccount, Long>{
             
	
}


