package com.example.demo.error;

import java.util.List;

public class BadRequest extends RuntimeException{
	
	private List<Error> errors;

	public List<Error> getErrors() {
		return errors;
	}

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

	public BadRequest(String message,List<Error> errors) {
		super(message);
		this.errors = errors;
	}

       
}
