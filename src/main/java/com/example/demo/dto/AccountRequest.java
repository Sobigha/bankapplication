package com.example.demo.dto;

import java.sql.Date;

public class AccountRequest {
  
	    private String accountHolderName;
	    private Date dob;
	    private String accountType;
	    private Double initialDeposit=0.0;
	    
	   
	    public String getAccountHolderName() {
			return accountHolderName;
		}
	    
		public void setAccountHolderName(String accountHolderName) {
			this.accountHolderName = accountHolderName;
		}
		
		public Date getDob() {
			return dob;
		}
		
		public void setDob(Date dob) {
			this.dob = dob;
		}
		
		public String getAccountType() {
			return accountType;
		}
		
		public void setAccountType(String accountType) {
			this.accountType = accountType;
		}
		
		public Double getInitialDeposit() {
			return initialDeposit;
		}
		
		public void setInitialDeposit(Double initialDeposit) {
			this.initialDeposit = initialDeposit;
		}
		
   
}
