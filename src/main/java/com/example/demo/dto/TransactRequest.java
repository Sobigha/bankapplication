package com.example.demo.dto;

public class TransactRequest {

	private Double amount;
	private String type;
	
	@Override
	public String toString() {
		return "Transact [amount=" + amount + ", type=" + type + "]";
	}
	
	public Double getAmount() {
		return amount;
	}
	
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	
}
