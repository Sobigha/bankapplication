package com.example.demo.dto;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.example.demo.model.Transaction;

public class AccountResponse {
     
			private Long accountNumber;	    
		  	private String accountHolderName;	  
		  	private Date dateOfBirth;	  
		  	private Double balance=0.0;	  	
		  	private Double transactionFee = 0.0;
		  	private String accountType;           
		  	private LocalDateTime createdAt;
		  	private LocalDateTime updatedAt;
//		  	private Optional<Transaction> transaction;
  
		 
//
//			public Optional<Transaction> getTransaction() {
//				return transaction;
//			}
//
//			public void setTransaction(Optional<Transaction> list) {
//				this.transaction = list;
//			}

			public Long getAccountNumber() {
				return accountNumber;
			}
		  	
			public void setAccountNumber(Long accountNumber) {
				this.accountNumber = accountNumber;
			}
			
			public String getAccountHolderName() {
				return accountHolderName;
			}
			
			public void setAccountHolderName(String accountHolderName) {
				this.accountHolderName = accountHolderName;
			}
			
			public Date getDateOfBirth() {
				return dateOfBirth;
			}
			
			public void setDateOfBirth(Date dateOfBirth) {
				this.dateOfBirth = dateOfBirth;
			}
			
			public Double getBalance() {
				return balance;
			}
			public void setBalance(Double balance) {
				this.balance = balance;
			}
			
			public Double getTransactionFee() {
				return transactionFee;
			}
			
			public void setTransactionFee(Double transactionFee) {
				this.transactionFee = transactionFee;
			}
			
			public String getAccountType() {
				return accountType;
			}
			
			public void setAccountType(String accountType) {
				this.accountType = accountType;
			}
			
			public LocalDateTime getCreatedAt() {
				return createdAt;
			}
			
			public void setCreatedAt(LocalDateTime createdAt) {
				this.createdAt = createdAt;
			}
			
			public LocalDateTime getUpdatedAt() {
				return updatedAt;
			}
			
			public void setUpdatedAt(LocalDateTime updatedAt) {
				this.updatedAt = updatedAt;
			}
			
}
