package com.example.demo.dto;

import java.time.LocalDateTime;

public class TransactResponse {
 
	    private Long transactionId;
	    private Long accountNumber;
	    private Double amount;
	    private String type;
	    private Double oldBalance;
	    private Double newBalance;
	    private LocalDateTime transactionDate;
	    private String transactionStatus;
	    private LocalDateTime createdAt;
	    private LocalDateTime updatedAt;
	     
		public Long getTransactionId() {
			return transactionId;
		}
		public void setTransactionId(Long transactionId) {
			this.transactionId = transactionId;
		}
		public Long getAccountNumber() {
			return accountNumber;
		}
		public void setAccountNumber(Long accountNumber) {
			this.accountNumber = accountNumber;
		}
		public Double getAmount() {
			return amount;
		}
		public void setAmount(Double amount) {
			this.amount = amount;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public Double getOldBalance() {
			return oldBalance;
		}
		public void setOldBalance(Double oldBalance) {
			this.oldBalance = oldBalance;
		}
		public Double getNewBalance() {
			return newBalance;
		}
		public void setNewBalance(Double newBalance) {
			this.newBalance = newBalance;
		}
		public LocalDateTime getTransactionDate() {
			return transactionDate;
		}
		public void setTransactionDate(LocalDateTime transactionDate) {
			this.transactionDate = transactionDate;
		}
		public String getTransactionStatus() {
			return transactionStatus;
		}
		public void setTransactionStatus(String transactionStatus) {
			this.transactionStatus = transactionStatus;
		}
		public LocalDateTime getCreatedAt() {
			return createdAt;
		}
		public void setCreatedAt(LocalDateTime createdAt) {
			this.createdAt = createdAt;
		}
		public LocalDateTime getUpdatedAt() {
			return updatedAt;
		}
		public void setUpdatedAt(LocalDateTime updatedAt) {
			this.updatedAt = updatedAt;
		}
	
	
}
